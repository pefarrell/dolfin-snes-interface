from dolfin import *
from petscinterface import *
import sys

# Pass PETSc arguments
args = sys.argv[:1] + """
                      --petsc.snes_type newtonls
                      --petsc.snes_monitor
                      --petsc.snes_max_it 100

                      --petsc.ksp_type preonly

                      --petsc.pc_type lu
                      --petsc.pc_factor_mat_solver_package mumps
                      """.split()
parameters.parse(args)

mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)
R = FunctionSpace(mesh, "R", 0)

u = Function(V)
v = TestFunction(V)

f = Constant(1.0)
g = Constant(0.0)
epsilon = Constant(1.0e-5)
p = interpolate(Constant(3), R)
bc = DirichletBC(V, g, DomainBoundary())

def gamma(u):
  return (epsilon**2 + 0.5 * inner(grad(u), grad(u)))**((p-2)/2)

F = inner(grad(v), gamma(u) * grad(u))*dx - inner(f, v)*dx

(snes, x) = configure_snes(F, u, bc)
snes.solve(None, x)
