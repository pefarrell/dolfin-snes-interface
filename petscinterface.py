from dolfin import *
from petsc4py import PETSc

class ForwardProblem(object):
    def __init__(self, F, u, bcs=None, P=None):
        self.F = F
        self.u = u
        self.bcs = bcs

        self.J = derivative(F, u)
        if P is not None:
            self.JP = derivative(P, u)

        # This object will handle assembly and boundary conditions
        self.assembler = SystemAssembler(self.J, self.F, self.bcs)
        if P is None:
            self.Passembler = None
        else:
            self.Passembler = SystemAssembler(self.JP, self.F, self.bcs)

        # DOLFIN vector for the solution
        self.u_dvec = as_backend_type(u.vector())
        # PETSc vector underlying it
        self.u_pvec = self.u_dvec.vec()

        # Mat and Vec for the Jacobian and residual
        self.b = self.u_pvec.duplicate()
        sizes = self.b.getSizes()

        # We need to initialise the matrix size and local-to-global maps
        A_dmat = PETScMatrix()
        self.assembler.init_global_tensor(A_dmat, Form(self.J))
        self.A = A_dmat.mat()

        if P is None:
            self.P = self.A # use the same matrix to build the preconditioner
        else:
            P_dmat = PETScMatrix()
            self.Passembler.init_global_tensor(P_dmat, Form(self.JP))
            self.P = P_dmat.mat()

    def update_x(self, x):
        """Given a PETSc Vec x, update the storage of our
           solution function u."""

        x.copy(self.u_pvec)
        self.u_dvec.update_ghost_values()

    def jacobian(self, snes, x, A, P):
        """The callback that the SNES executes to assemble our Jacobian."""

        self.update_x(x)
        A_wrap = PETScMatrix(A)
        self.assembler.assemble(A_wrap)

        if self.Passembler is not None:
            P_wrap = PETScMatrix(P)
            self.Passembler.assemble(P_wrap)

    def residual(self, snes, x, b):
        """The callback that the SNES executes to compute the residual."""

        self.update_x(x)
        b_wrap = PETScVector(b)
        self.assembler.assemble(b_wrap, self.u_dvec)

def configure_snes(F, u, bcs, P=None):
    problem = ForwardProblem(F, u, bcs=bcs, P=P)
    snes    = PETSc.SNES().create()

    snes.setFunction(problem.residual, problem.b)
    snes.setJacobian(problem.jacobian, problem.A, problem.P)
    snes.setFromOptions()

    if snes.getType() == "composite":
        num = snes.getCompositeNumber()
        for i in range(num):
            subsnes = snes.getCompositeSNES(i)
            subsnes.setFunction(problem.residual, problem.b)
            subsnes.setJacobian(problem.jacobian, problem.A, problem.P)

    # we also need temporary storage for the SNES solution;
    # copy the PETSc vector underlying u
    x = problem.u_pvec.copy()

    return (snes, x)

def vec(x):
    if isinstance(x, cpp.Function):
        x = x.vector()
    if isinstance(x, cpp.GenericVector):
        x = as_backend_type(x)

    if isinstance(x, PETScVector):
        return x.vec()
    else:
        raise ValueError("don't know how to take the vector of this")
